from django.db import models
#IMPORT FROM SUPERUSER
from django.contrib.auth.models import User
#from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import AnonymousUser


# Create your models here.

#CLASS OF NOTES WITH INHERITANCE OF MODEL
class Notes(models.Model):
      user = models.ForeignKey(User, on_delete=models.CASCADE)
      title = models.CharField(max_length=200)
      description=models.TextField()
      #portfolio_site = models.URLField(blank=True)
    
      #FUNCTION TO DISPLAY TITLE OF NOTES
      def __str__(self):
          return self.title

      #CLASS TO REMOVE EXTRA 'S' FROM NOTES
      class Meta:
            verbose_name = "notes"
            verbose_name_plural = "notes"


class Homework(models.Model):
      user = models.ForeignKey(User, on_delete=models.CASCADE)
      subject = models.CharField(max_length=200)
      title = models.CharField(max_length=200)
      description=models.TextField()
      due = models.DateTimeField()
      is_finished = models.BooleanField(default=False)

    
    #FUNCTION TO DISPLAY TITLE OF HOMEWORK
      def __str__(self):
          return self.title


class Todo(models.Model):
      user = models.ForeignKey(User, on_delete=models.CASCADE)
      title = models.CharField(max_length=200)
      is_finished = models.BooleanField(default=False)


    #FUNCTION TO DISPLAY TITLE OF TODOS
      def __str__(self):
          return self.title
