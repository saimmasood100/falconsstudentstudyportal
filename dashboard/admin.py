from django.contrib import admin
#IMPORT ALL MODELS FROM CURRENT APPLICATION
from . models import *
from django.contrib.auth.models import AnonymousUser


# Register your models here.
admin.site.register(Notes)
admin.site.register(Homework)
admin.site.register(Todo)