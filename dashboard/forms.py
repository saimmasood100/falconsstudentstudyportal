from django import forms
from django.db.models import fields
from django.forms import widgets
from . models import *
#IMPORTING USER REGISTRATION FORM
from django.contrib.auth.forms import UserCreationForm


class NotesForm(forms.ModelForm):
      class Meta():
            model = Notes #INHERIT FROM META CLASS OF NOTES
            fields = ['title', 'description']


class DateInput(forms.DateInput):
      input_type = 'date'


class HomeworkForm(forms.ModelForm):
      class Meta():
            model = Homework #INHERIT FROM META CLASS OF HOMEWORK
            widgets = {'due':DateInput()}
            fields = ['subject', 'title', 'description', 'due', 'is_finished']

#TEXTBOX CLASS
class DashboardForm(forms.Form):
      text = forms.CharField(max_length=100,label="ENTER YOUR SEARCH ")

class TodoForm(forms.ModelForm):
      class Meta():
            model = Todo
            fields = ['title', 'is_finished']

#CLASS FOR CHOICES OF CONVERSIONS
class ConversionForm(forms.Form):
      CHOICES = [('length','Length'),('mass','Mass')]
      measurement = forms.ChoiceField(choices = CHOICES, widget=forms.RadioSelect)


#CLASS FOR LENGTH CONVERSION CHOICES
class ConversionLengthForm(forms.Form):
      CHOICES = [('yard','Yard'),('foot','Foot')]
      input = forms.CharField(required=False, label=False, widget=forms.TextInput(
            attrs = {'type':'number','placeholder':'Enter The Number'}
      ))
      measure1 = forms.CharField(
            label = '',widget = forms.Select(choices = CHOICES)
      )
      measure2 = forms.CharField(
            label = '',widget = forms.Select(choices = CHOICES)
      )

#CLASS FOR MASS CONVERSION CHOICES
class ConversionMassForm(forms.Form):
      CHOICES = [('pound','Pound'),('kilogram','Kilogram')]
      input = forms.CharField(required=False, label=False, widget=forms.TextInput(
            attrs = {'type':'number','placeholder':'Enter The Number'}
      ))
      measure1 = forms.CharField(
            label = '',widget = forms.Select(choices = CHOICES)
      )
      measure2 = forms.CharField(
            label = '',widget = forms.Select(choices = CHOICES)
      )


class UserRegistrationForm(UserCreationForm):
      class Meta:
            model = User
            fields = ['username', 'password1', 'password2']